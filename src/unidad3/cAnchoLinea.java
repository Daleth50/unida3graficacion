package unidad3;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class cAnchoLinea  extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8776195607891916536L;
	JLabel etiq;
	JTextField ct;
	JButton Ac, Ca;
	int res = -1;

	public cAnchoLinea(JFrame f, boolean modal, String tit)
	{
		super(f, modal);
		setLayout(new FlowLayout(FlowLayout.LEFT));
		setSize(500, 90);
		setLocationRelativeTo(f);
		etiq = new JLabel(tit);
		ct = new JTextField(5);
		Ac = new JButton("Aceptar");
		Ca = new JButton("Cancelar");
		add(etiq);
		add(ct);
		add(Ac);
		add(Ca);
		Ac.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String v = ct.getText();
				if (!v.equals(""))
				{
					res = Integer.parseInt(v);
				} else
				{
					res = -1;

				}
				setVisible(false);
				dispose();
			}
		});

		Ca.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				res = -1;
				setVisible(false);
				dispose();
			}
		});
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	}

	public int mostrar()
	{
		setVisible(true);
		return res;
	}

}

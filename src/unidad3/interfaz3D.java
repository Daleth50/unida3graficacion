package unidad3;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.net.URL;
import java.util.Random;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSlider;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class interfaz3D extends JPanel implements ChangeListener, MouseWheelListener {
	JFrame V;
	figura3D F;
	JSlider s1, s2, s3, s4, s5;
	float cd = 0f;
	int cont = 0;
	int cont2 = 0;
	JPopupMenu popup;
	JMenuItem restaurar;
	JButton b1, b2, b3, b4, b5, b6, b7, b8, b9, b10;
	JToolBar barraH;
	Action a1, a2, a3, a4, a5, a6, a7, a8, a9, a10;
	public int val = 100;
	boolean mover = false;
	public interfaz3D(float f[][], int s[]) {
		V = new JFrame("Transformaciones en 3D");
		V.setSize(800, 700);
		V.add(this, BorderLayout.CENTER);
		popup = new JPopupMenu();
		restaurar = new JMenuItem("Restaurar");
		popup.add(restaurar);
		
		s1 = new JSlider(JSlider.HORIZONTAL, 0, 360, 0);
		s1.setMinorTickSpacing(25);
		s1.setMajorTickSpacing(50);
		s1.setPaintTicks(true);
		s1.setPaintLabels(true);

		s2 = new JSlider(JSlider.HORIZONTAL, 0, 360, 0);
		s2.setMinorTickSpacing(25);
		s2.setMajorTickSpacing(50);
		s2.setPaintTicks(true);
		s2.setPaintLabels(true);

		s3 = new JSlider(JSlider.HORIZONTAL, 0, 360, 0);
		s3.setMinorTickSpacing(25);
		s3.setMajorTickSpacing(50);
		s3.setPaintTicks(true);
		s3.setPaintLabels(true);
		
		s4 = new JSlider(JSlider.VERTICAL, 1, 20, 1);
		s4.setMinorTickSpacing(1);
		s4.setMajorTickSpacing(5);
		s4.setPaintTicks(true);
		s4.setPaintLabels(true);
		
		s5 = new JSlider(JSlider.VERTICAL, 0, 100, val);
		s5.setMinorTickSpacing(5);
		s5.setMajorTickSpacing(25);
		s5.setPaintTicks(true);
		s5.setPaintLabels(true);		

		JPanel abajo = new JPanel(new GridLayout(1, 3));
		abajo.add(s1);
		abajo.add(s2);
		abajo.add(s3);

		V.add(abajo, BorderLayout.SOUTH);
		
		JPanel derecha = new JPanel(new GridLayout(2, 1));
		derecha.add(s5);
		derecha.add(s4);

		V.add(derecha, BorderLayout.EAST);
		F = new figura3D(f, s);
		s1.addChangeListener(this);
		s2.addChangeListener(this);
		s3.addChangeListener(this);
		s4.addChangeListener(this);
		s5.addChangeListener(this);
		addMouseWheelListener(this);
		
		// Barra de Herramientas
				barraH = new JToolBar("Operaciones frecuentes", JToolBar.VERTICAL);
				V.add(barraH, BorderLayout.WEST);
				URL ruta = getClass().getResource("/unidad3/rec/restaurar.png");
				a1 = new AbstractAction("", new ImageIcon(ruta)) {
					private static final long serialVersionUID = -6454166177353073228L;

					public void actionPerformed(ActionEvent e) {
						 F.Restaurar();
						 s1.setValue(0);
						 s2.setValue(0);
						 s3.setValue(0);
						 s4.setValue(1);
						 s5.setValue(100);
						repaint();
					}
				};

				a1.putValue(Action.SHORT_DESCRIPTION, "Restaurar la figura a las coordenada originales");
				b1 = new JButton(a1);
				barraH.add(b1);
				barraH.setFloatable(false);

				ruta = getClass().getResource("/unidad3/rec/ancholinea.png");
				a2 = new AbstractAction("", new ImageIcon(ruta)) {
					private static final long serialVersionUID = 3777891264055114856L;

					public void actionPerformed(ActionEvent e) {
						cAnchoLinea obj = new cAnchoLinea(V, true, "Dame el ancho de linea");
						int res = obj.mostrar();
						if (res != -1) {
							F.AnchoLinea(res);
						}
						repaint();
					}
				};
				a2.putValue(Action.SHORT_DESCRIPTION, "Cambia ancho de linea");
				b2 = new JButton(a2);
				barraH.add(b2);

				ruta = getClass().getResource("/unidad3/rec/gradientes1.png");
				a3 = new AbstractAction("", new ImageIcon(ruta)) {
					private static final long serialVersionUID = -2429841061784073074L;

					public void actionPerformed(ActionEvent e) {
						F.AplicaGradiente(1, V);
						repaint();
					}
				};
				a3.putValue(Action.SHORT_DESCRIPTION, "Aplica gradiente");
				b3 = new JButton(a3);
				barraH.add(b3);

				ruta = getClass().getResource("/unidad3/rec/gradientes2.png");
				a4 = new AbstractAction("", new ImageIcon(ruta)) {
					private static final long serialVersionUID = -2429841061784073074L;

					public void actionPerformed(ActionEvent e) {
						F.AplicaGradiente(2, V);
						repaint();
					}
				};
				a4.putValue(Action.SHORT_DESCRIPTION, "Aplica gradiente");
				b4 = new JButton(a4);
				barraH.add(b4);

				ruta = getClass().getResource("/unidad3/rec/gradientes3.png");
				a5 = new AbstractAction("", new ImageIcon(ruta)) {
					private static final long serialVersionUID = -2429841061784073074L;

					public void actionPerformed(ActionEvent e) {
						F.AplicaGradiente(3, V);
						repaint();
					}
				};
				a5.putValue(Action.SHORT_DESCRIPTION, "Aplica gradiente");
				b5 = new JButton(a5);
				barraH.add(b5);

				ruta = getClass().getResource("/unidad3/rec/gradientes4.png");
				a6 = new AbstractAction("", new ImageIcon(ruta)) {
					private static final long serialVersionUID = -2429841061784073074L;

					public void actionPerformed(ActionEvent e) {
						F.AplicaGradiente(4, V);
						repaint();
					}
				};
				a6.putValue(Action.SHORT_DESCRIPTION, "Aplica gradiente");
				b6 = new JButton(a6);
				barraH.add(b6);

				ruta = getClass().getResource("/unidad3/rec/gradientes5.png");
				a7 = new AbstractAction("", new ImageIcon(ruta)) {
					private static final long serialVersionUID = -2429841061784073074L;

					public void actionPerformed(ActionEvent e) {
						F.AplicaGradiente(5, V);
						repaint();
					}
				};
				a7.putValue(Action.SHORT_DESCRIPTION, "Aplica gradiente");
				b7 = new JButton(a7);
				barraH.add(b7);

				ruta = getClass().getResource("/unidad3/rec/terminacionunion.png");
				a8 = new AbstractAction("", new ImageIcon(ruta)) {
					private static final long serialVersionUID = -2429841061784073074L;

					public void actionPerformed(ActionEvent e) {
						Random r = new Random();
						int termination = r.nextInt(3);
						int union = r.nextInt(3);
						F.cambiaStroke(termination, union);
						repaint();
					}
				};
				a8.putValue(Action.SHORT_DESCRIPTION, "Cambia terminaciones y uniones");
				b8 = new JButton(a8);
				barraH.add(b8);

				ruta = getClass().getResource("/unidad3/rec/reflexion.png");
				a10 = new AbstractAction("", new ImageIcon(ruta)) {
					private static final long serialVersionUID = 7131560757025130861L;

					public void actionPerformed(ActionEvent e) {
						cRefleccion obj = new cRefleccion(V, true);
						int res[] = obj.mostrar();
						if (res != null) {
							F.refleccionH(res[0], res[1]);
							repaint();
						}
					}
				};
				a10.putValue(Action.SHORT_DESCRIPTION, "Reflexión");
				b10 = new JButton(a10);
				barraH.add(b10);
				
				ruta = getClass().getResource("/unidad3/rec/s.png");
				a9 = new AbstractAction("", new ImageIcon(ruta)) {
					private static final long serialVersionUID = 7131560757025130861L;

					public void actionPerformed(ActionEvent e) {
						System.exit(0);
					}
				};
				a9.putValue(Action.SHORT_DESCRIPTION, "Salir");
				b9 = new JButton(a9);
				barraH.add(b9);
		
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if(e.getButton() == 2) {
					if(cont == 6 ) {
						cd = .2f;
						cont2++;
                        if (cont2 == 6) {
                            cont = 0;
                        }
					} else {
						cont2 = 0;
                        cont++;
                        cd = -.2f;
					}
					F.deformarH(cd);
					repaint();
				}
				if(e.getButton() == MouseEvent.BUTTON3){
					muestraMenu(e);
				}
				else {
					ocultaMenu(e);
				}
				int cx = e.getX();
				int cy = e.getY();
				mover = F.verificar(cx, cy);
			}
		});

		addMouseMotionListener(new MouseMotionAdapter() {

			@Override
			public void mouseDragged(MouseEvent e) {
				if (mover) {
					int cx = e.getX();
					int cy = e.getY();
					F.actualizarPos(cx, cy);
					repaint();
				}
			}
		});
		
		restaurar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				F.Restaurar();
				 s1.setValue(0);
				 s2.setValue(0);
				 s3.setValue(0);
				 s4.setValue(1);
				 s5.setValue(100);
				repaint();
				popup.setVisible(false);
			}
		});
				
		V.setVisible(true);
		V.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		F.conv2D();
		F.dibujar(g);
		F.vistaSuperior(g);
		F.vistaLateral(g);
		F.vistaFrontal(g);
	}

	public static void main(String[] args) {
		float letra[][] = { { -10, 30, 0 }, // A 0
				{ 20, 30, 0 }, // B 1
				{ 30, 20, 0 }, // C 2
				{ 30, 0, 0 }, // D 3
				{ 20, -10, 0 }, // E 4
				{ 0, -10, 0 }, // F 5
				{ 0, -30, 0 }, // G 6
				{ -10, -30, 0 }, // H 7
				{ 0, 0, 0 }, // I 8
				{ 10, 0, 0 }, // J 9
				{ 10, 20, 0 }, // K 10
				{ 0, 20, 0 }, // L 11
				{ -10, -30, -10 }, // M 12
				{ -10, 30, -10 }, // N 13
				{ 20, 30, -10 }, // O 14
				{ 30, 20, -10 }, // P 15
				{ 30, 0, -10 }, // Q 16
				{ 20, -10, -10 }, // R 17
				{ 0, -10, -10 }, // S 18
				{ 0, -30, -10 }, // T 19
				{ 0, 0, -10 }, // U 20
				{ 10, 0, -10 }, // V 21
				{ 10, 20, -10 }, // W 22
				{ 0, 20, -10 }// X 23
		};
		int secLetra[] = { 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 0, 8, 9, 9, 10, 10, 11, 11, 8, 12, 13, 13, 14,
				14, 15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 12, 20, 21, 21, 22, 22, 23, 23, 20, 0, 13, 1, 14, 2, 15, 3,
				16, 4, 17, 5, 18, 6, 19, 7, 12, 8, 20, 9, 21, 10, 22, 11, 23 };
		
		new interfaz3D(letra, secLetra);
		new ayuda();

	}

	@Override
	public void stateChanged(ChangeEvent e) {
		int gradosX = s1.getValue();
		int gradosY = s2.getValue();
		int gradosZ = s3.getValue();
		F.rotacionXYZH(gradosX, gradosY, gradosZ);
		
		 if (e.getSource() == s4) {
			 int tam = s4.getValue();
			 F.escalar(tam, tam);
		 }
		 if(e.getSource() == s5) {
		 double val = (double) s5.getValue() / 100;
			float alpha = (float) val;
			F.cambiaAlpha(alpha);
		 }
		repaint();
	}
	
	public void mouseWheelMoved(MouseWheelEvent e) {
		int rotacion = e.getWheelRotation();
		if (rotacion < 0) { // arriba
			s4.setValue(s4.getValue() + 1);
		} else {
			s4.setValue(s4.getValue() - 1);
		}
		repaint();
		// abajo
	}
	
	private void muestraMenu(MouseEvent e) {
		// isPopupTrigger() indica si es el evento de raton
		// por defecto en el sistema operativo para mostrar
		// el menu.
		if (e.isPopupTrigger()) {
			popup.setLocation(e.getLocationOnScreen());
			popup.setVisible(true);
		}
	}
	private void ocultaMenu(MouseEvent e) {
		
		popup.setVisible(false);
		
	}

}

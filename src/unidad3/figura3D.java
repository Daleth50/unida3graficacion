package unidad3;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.GeneralPath;

import javax.swing.JFrame;


public class figura3D {
	float fig[][], fig2D[][], orig[][];
	int sec[];
	int distancia = 1000, mz = -350;
	int Tx;
	int Ty;
	int size = 1;
	float alpha = 1.0F;
	Paint pa = Color.BLACK;
	Shape s, original;
	Graphics2D g2d;
	int[] terminations = { BasicStroke.CAP_SQUARE, BasicStroke.CAP_BUTT, BasicStroke.CAP_ROUND };
	int[] joins = { BasicStroke.JOIN_MITER, BasicStroke.JOIN_BEVEL, BasicStroke.JOIN_ROUND };
	BasicStroke stroke = new BasicStroke(size, terminations[0], joins[0]);

	public figura3D(float f[][], int s[]) {
		fig = f;
		sec = s;
		Tx = 350;
		Ty = 350;
		fig2D = new float[f.length][2];
		orig = new float[f.length][3];

		for (int i = 0; i < f.length; i++) {
			orig[i][0] = f[i][0];
			orig[i][1] = f[i][1];
			orig[i][2] = f[i][2];
		}
	}

	public void conv2D() {
		// Tx = tx;
		// Ty = ty;
		for (int i = 0; i < fig.length; i++) {
			fig2D[i][0] = (distancia * fig[i][0]) / (fig[i][2] + mz);
			fig2D[i][1] = (distancia * fig[i][1]) / (fig[i][2] + mz);
			fig2D[i][0] += Tx;
			fig2D[i][1] += Ty;
		}
	}

	public void dibujar(Graphics g) {
		g2d = (Graphics2D) g;
		g2d.setStroke(stroke);
		g2d.setPaint(pa);
		g2d.setComposite(createComposite(alpha));
		for (int i = 0; i < sec.length; i += 2) {
			g2d.drawLine((int) fig2D[sec[i]][0], (int) fig2D[sec[i]][1], (int) fig2D[sec[i + 1]][0],
					(int) fig2D[sec[i + 1]][1]);	
		}
	}
	private AlphaComposite createComposite(float alfa) {
		int tipo = AlphaComposite.SRC_OVER;

		return (AlphaComposite.getInstance(tipo, alfa));
	}

	public float[][] rotacionY(int grados) {
		double gr = Math.toRadians(grados);
		float cosr = (float) Math.cos(gr);
		float sinr = (float) Math.sin(gr);
		float res[][] = new float[orig.length][2];
		for (int i = 0; i < fig.length; i++) {
			float x = orig[i][0];
			res[i][1] = -orig[i][1];
			res[i][0] = x * cosr + orig[i][2] * sinr;
		}
		return res;
	}

	public float[][] rotacionX(int grados) {
		double gr = Math.toRadians(grados);
		float cosr = (float) Math.cos(gr);
		float sinr = (float) Math.sin(gr);
		float ft[][] = new float[orig.length][2];
		for (int i = 0; i < fig.length; i++) {
			float y = orig[i][1];
			ft[i][0] = -orig[i][0];
			ft[i][1] = y * cosr + orig[i][2] * sinr;
			// fig[i][2] = -y * sinr + orig[i][2] * cosr;
		}
		return ft;
	}

	public float[][] rotacionZ(float v[][], int grados) {
		double gr = Math.toRadians(grados);
		float cosr = (float) Math.cos(gr);
		float sinr = (float) Math.sin(gr);
		float res[][] = new float[v.length][3];
		for (int i = 0; i < fig.length; i++) {
			float x = v[i][0];
			res[i][2] = v[i][2];
			res[i][0] = x * cosr + v[i][1] * sinr;
			res[i][1] = -x * sinr + v[i][1] * cosr;
		}
		return res;
	}

	public void rotacionXYZ(int gx, int gy, int gz) {
		float res[][] = rotacionX(gx);
		// res = rotacionY(res, gy);
		res = rotacionZ(res, gz);
		fig = res;
	}

	public void rotacionXYZH(int gx, int gy, int gz) {
		double grX=Math.toRadians((double)gx);
		double grY=Math.toRadians((double)gy);
		double grZ=Math.toRadians((double)gz);
		float cosx=(float)Math.cos(grX);
		float sinx=(float)Math.sin(grX);
		float cosy=(float)Math.cos(grY);
		float siny=(float)Math.sin(grY);
		float cosz=(float)Math.cos(grZ);
		float sinz=(float)Math.sin(grZ);
		
		for (int i = 0; i < fig.length; i++) 
		{
			float x= orig[i][0];
			float y= orig[i][1];
			float z= orig[i][2];
			fig[i][0]=x*(cosy*cosz)+y*((-sinx)*siny*cosz+cosx*sinz)+z*(cosx*siny*cosz+sinx*sinz);
			fig[i][1]=x*(cosy*(-sinz))+y*((-sinx)*siny*(-sinz)+cosz*cosx)+z*(cosx*siny*(-sinz)+sinx*cosz);
			fig[i][2]=x*(-siny)+y*((-sinx)*cosy)+z*(cosx*cosy);
		}
	}

	public void vistaSuperior(Graphics g) {
		float res[][] = rotacionX(90);
		// dibujar la figura
		int px = 80;
		int py = 80;
		for (int i = 0; i < res.length; i++) {
			res[i][0] += px;
			res[i][1] += py;
		}
		dibujar(res, g, "Vista superior", 50, 40);
	}

	public void vistaLateral(Graphics g) {
		float res[][] = rotacionY(90);
		// dibujar la figura
		int px = 80;
		int py = 180;
		for (int i = 0; i < res.length; i++) {
			res[i][0] += px;
			res[i][1] += py;
		}
		dibujar(res, g, "Vista lateral", 50, 120);
	}

	public void vistaFrontal(Graphics g) {
		float res[][] = rotacionX(180);
		// dibujar la figura
		int px = 80;
		int py = 300;
		for (int i = 0; i < res.length; i++) {
			res[i][0] += px;
			res[i][1] += py;
		}
		dibujar(res, g, "Vista frontal", 50, 250);
	}

	private void dibujar(float[][] res, Graphics g, String mens, int px, int py) {
		g.drawString(mens, px, py);
		for (int i = 0; i < sec.length; i += 2) {
			g.drawLine((int) res[sec[i]][0], (int) res[sec[i]][1], (int) res[sec[i + 1]][0], (int) res[sec[i + 1]][1]);
		}
	}

	public boolean verificar(int cx, int cy) {
		int minx=(int)fig2D[0][0],miny=(int)fig2D[0][1],maxx=(int)fig2D[0][0],maxy=(int)fig2D[0][1];
		for (int i = 0; i < fig2D.length; i++) {
			if(fig2D[i][0]<minx) minx=(int)fig2D[i][0];
			if(fig2D[i][1]<miny) miny=(int)fig2D[i][1];
			if(fig2D[i][0]>maxx) maxx=(int)fig2D[i][0];
			if(fig2D[i][1]>maxy) maxy=(int)fig2D[i][1];
		}
		return (cx>=minx &&  cx<=maxx && cy>=miny && cy<=maxy) ? true : false ;
	}

	public void actualizarPos(int cx, int cy) {
		Tx = cx;
		Ty = cy;
	}
	
	public void escalar(double A, double B)
	{	//xA,yB
		for (int i = 0; i < fig.length; i++) 
		{
			fig[i][0]*=A;
			fig[i][1]*=B;
		}
	}
	
	public void Traslacion(int Tx, int Ty)
	{
		for (int i = 0; i < fig.length; i++) 
		{
			fig[i][0]+=Tx;
			fig[i][1]+=Ty;
		}
	}
	
	public void deformarH(float cd)
	{
		float Tx=(int)fig[0][0];
		float Ty=(int)fig[0][1];
		for (int i = 0; i < fig.length; i++) 
		{
			double x=fig[i][0];
			double y=fig[i][1];
			fig[i][0]=(float) (x+cd*y-cd*Ty);
			fig[i][1]=(float) (cd*x+y-cd*Tx);
			
		}
	}
	
	public void Restaurar()
	{
		for (int i = 0; i < orig.length; i++) {
			fig[i][0]=orig[i][0];
			fig[i][1]=orig[i][1];
			fig[i][2]=orig[i][2];
			
		}
		size = 1;
		pa = Color.BLACK;
		stroke = new BasicStroke(size, terminations[0], joins[0]);
		alpha = 1.0F;
	}
	
	public void refleccionH(float rX, float rY) {
		int Tx = (int) fig[0][0];
		int Ty = (int) fig[0][1];
		for (int i = 0; i < fig.length; i++) {
			double x = fig[i][0];
			double y = fig[i][1];
			fig[i][0] = (float) (rX * x - Tx * rX + Tx);
			fig[i][1] = (float) (rY * y - Ty * rY + Ty);
		}
	}
	public void AnchoLinea(int tam) {
		size = tam;
		stroke = new BasicStroke(tam, terminations[0], joins[0]);
	}
	
	public void AplicaGradiente(int g, JFrame v) {
		if (g == 1) {
			pa = new GradientPaint(0, 0, Color.RED, v.getWidth(), 0, Color.BLUE);
		}
		if (g == 2) {
			pa = new GradientPaint(0, 0, Color.GREEN, 0, v.getHeight(), Color.YELLOW);
		}
		if (g == 3) {
			pa = new GradientPaint(0, 0, Color.GRAY, v.getWidth() / 2, v.getHeight() / 2, Color.ORANGE);
		}
		if (g == 4) {
			pa = new GradientPaint(0, 0, Color.RED, 20, 0, Color.BLUE, true);
		}
		if (g == 5) {
			pa = new GradientPaint(0, 0, Color.GREEN, 0, 20, Color.YELLOW, true);
		}
	}
	
	public void cambiaStroke(int termination, int union) {
		stroke = new BasicStroke(size, terminations[termination], joins[union]);
	}
	
	public void cambiaAlpha(float alfa) {
		alpha = alfa;
	}

}

package unidad3;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class ayuda extends JFrame {
	private static final long serialVersionUID = 7917227069231703359L;
	JLabel instrucciones;
	String txt = "<html><body>Boton derecho del mouse abre popup para restaurar la figura<br>"
			+ "Click en la rueda del mouse deforma la figura<br>"
			+ "Del lado izquierdo se encuentran algunas herramientas para cambiar la apariencia asi como un boton para aplicar la reflexion<br>"
			+ "Los Sliders de la parte inferior aplican rotacion en X,Y,Z respectivamente<br>"
			+ "El Slider superior de la parte derecha controla la transparencia de la figura<br>"
			+ "El slider inferior de la parte derecha aplica un escalamiento<br>"
			+ "Al arrastrar la figura con el mouse le aplica una trasnalcion<br>"
			+ "</body></html>";

	public ayuda() {
		instrucciones = new JLabel(txt);
		setTitle("Ayuda");
		setSize(300, 400);
		setResizable(false);
		setLocationRelativeTo(null);
		add(instrucciones, BorderLayout.CENTER);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public static void main(String[] args) {
		new ayuda();
	}

}
